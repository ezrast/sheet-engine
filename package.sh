#!/bin/bash
set -euo pipefail
yarn
/bin/bash ./build.sh

mkdir public
mkdir public/node_modules
cp html/index.html public/
cp -rL html/assets public/
cp -rL html/js-modules public/
cp -r lib/es6_global/js public/
cp -r node_modules/bs-* public/node_modules/
