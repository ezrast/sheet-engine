let source = `
#@debug class_boost boost_Other

_Ability is .None .Str .Dex .Con .Int .Wis .Cha
_Ancestry is .Other .Human .Elf .Dwarf .Goblin
_Class is .Other .Alchemist .Barbarian .Bard

.row{
  .column1{
    Ancestry = [_Ancestry]
    Class = [_Class]
    Level = []

    abilities {
      .ability.row{
        Str = base_strength / 2 - 5
        base_strength "<<" = 10 + other_strength + str_boosts
        str_boosts "+" = 2 * ((free_boost1 == .Str) + (free_boost2 == .Str) + (free_boost3 == .Str) + (free_boost4 == .Str) + (ancestry_boost1 == .Str) + (ancestry_boost2 == .Str) + (ancestry_boost3 == .Str) - (ancestry_flaw == .Str) + (background_boost1 == .Str) + (background_boost2 == .Str) + (class_boost == .Str))
        other_strength "+" = []
      }
      .ability.row{
        Dex = base_dexterity / 2 - 5
        base_dexterity "<<" = 10 + other_dexterity + dex_boosts
        dex_boosts "+" = 2 * ((free_boost1 == .Dex) + (free_boost2 == .Dex) + (free_boost3 == .Dex) + (free_boost4 == .Dex) + (ancestry_boost1 == .Dex) + (ancestry_boost2 == .Dex) + (ancestry_boost3 == .Dex) - (ancestry_flaw == .Dex) + (background_boost1 == .Dex) + (background_boost2 == .Dex) + (class_boost == .Dex))
        other_dexterity "+" = []
      }
      .ability.row{
        Con = base_constitution / 2 - 5
        base_constitution "<<" = 10 + other_constitution + con_boosts
        con_boosts "+" = 2 * ((free_boost1 == .Con) + (free_boost2 == .Con) + (free_boost3 == .Con) + (free_boost4 == .Con) + (ancestry_boost1 == .Con) + (ancestry_boost2 == .Con) + (ancestry_boost3 == .Con) - (ancestry_flaw == .Con) + (background_boost1 == .Con) + (background_boost2 == .Con) + (class_boost == .Con))
        other_constitution "+" = []
      }
      .ability.row{
        Int = base_intelligence / 2 - 5
        base_intelligence "<<" = 10 + other_intelligence + int_boosts
        int_boosts "+" = 2 * ((free_boost1 == .Int) + (free_boost2 == .Int) + (free_boost3 == .Int) + (free_boost4 == .Int) + (ancestry_boost1 == .Int) + (ancestry_boost2 == .Int) + (ancestry_boost3 == .Int) - (ancestry_flaw == .Int) + (background_boost1 == .Int) + (background_boost2 == .Int) + (class_boost == .Int))
        other_intelligence "+" = []
      }
      .ability.row{
        Wis = base_wisdom / 2 - 5
        base_wisdom "<<" = 10 + other_wisdom + wis_boosts
        wis_boosts "+" = 2 * ((free_boost1 == .Wis) + (free_boost2 == .Wis) + (free_boost3 == .Wis) + (free_boost4 == .Wis) + (ancestry_boost1 == .Wis) + (ancestry_boost2 == .Wis) + (ancestry_boost3 == .Wis) - (ancestry_flaw == .Wis) + (background_boost1 == .Wis) + (background_boost2 == .Wis) + (class_boost == .Wis))
        other_wisdom "+" = []
      }
      .ability.row{
        Cha = base_charisma / 2 - 5
        base_charisma "<<" = 10 + other_charisma + cha_boosts
        cha_boosts "+" = 2 * ((free_boost1 == .Cha) + (free_boost2 == .Cha) + (free_boost3 == .Cha) + (free_boost4 == .Cha) + (ancestry_boost1 == .Cha) + (ancestry_boost2 == .Cha) + (ancestry_boost3 == .Cha) - (ancestry_flaw == .Cha) + (background_boost1 == .Cha) + (background_boost2 == .Cha) + (class_boost == .Cha))
        other_charisma "+" = []
      }
      Boosts{
        .row{
          ancestry_boost1 "Ancestry boosts:" = [_Ability]
          ancestry_boost2 "" = boost2_{Ancestry}
          ancestry_boost3 "" = boost3_{Ancestry}
        }
        ancestry_flaw "Ancestry flaw:" = flaw_{Ancestry}
        .row{
          background_boost1 "Background boosts:" = [_Ability]
          background_boost2 "" = [_Ability]
        }
        class_boost "Class boost:" = boost_{Class}
        .row{
          free_boost1 "Free Boosts:" = [_Ability]
          free_boost2 "" = [_Ability]
          free_boost3 "" = [_Ability]
          free_boost4 "" = [_Ability]
        }
      }
      #str: d20 + str
      #dex: d20 + dex
      #con: d20 + con
      #int: d20 + int
      #wis: d20 + wis
      #cha: d20 + cha
      #
    }
  }
  .column2{
    skills {
      strength {
        .row{
          AthleticsProf "" = [_Proficiency]
          Athletics = prof_{AthleticsProf} + Str
        }
      }

      dexterity {
        .row{
          AcrobaticsProf "" = [_Proficiency]
          Acrobatics = prof_{AcrobaticsProf} + Dex
        }
        .row{
          StealthProf "" = [_Proficiency]
          Stealth = prof_{StealthProf} + Dex
        }
        .row{
          ThieveryProf "" = [_Proficiency]
          Thievery = prof_{ThieveryProf} + Dex
        }
      }

      intelligence {
        .row{
          ArcanaProf "" = [_Proficiency]
          Arcana = prof_{ArcanaProf} + Int
        }
        .row{
          CraftingProf "" = [_Proficiency]
          Crafting = prof_{CraftingProf} + Int
        }
        .row{
          OccultismProf "" = [_Proficiency]
          Occultism = prof_{OccultismProf} + Int
        }
        .row{
          SocietyProf "" = [_Proficiency]
          Society = prof_{SocietyProf} + Int
        }
      }

      wisdom {
        .row{
          MedicineProf "" = [_Proficiency]
          Medicine = prof_{MedicineProf} + Wis
        }
        .row{
          NatureProf "" = [_Proficiency]
          Nature = prof_{NatureProf} + Wis
        }
        .row{
          ReligionProf "" = [_Proficiency]
          Religion = prof_{ReligionProf} + Wis
        }
        .row{
          SurvivalProf "" = [_Proficiency]
          Survival = prof_{SurvivalProf} + Wis
        }
      }

      charisma {
        .row{
          DeceptionProf "" = [_Proficiency]
          Deception = prof_{DeceptionProf} + Cha
        }
        .row{
          DiplomacyProf "" = [_Proficiency]
          Diplomacy = prof_{DiplomacyProf} + Cha
        }
        .row{
          IntimidationProf "" = [_Proficiency]
          Intimidation = prof_{IntimidationProf} + Cha
        }
        .row{
          PerformanceProf "" = [_Proficiency]
          Performance = prof_{PerformanceProf} + Cha
        }
      }
    }
  }
}

_ {
  boost2_Human = [_Ability]
  boost3_Human = .None
  flaw_Human = .None

  boost2_Elf = .Dex
  boost3_Elf = .Int
  flaw_Elf = .Con

  boost2_Dwarf = .Con
  boost3_Dwarf = .Wis
  flaw_Dwarf = .Cha

  boost2_Goblin = .Dex
  boost3_Goblin = .Cha
  flaw_Goblin = .Wis

  boost2_Other = [_Ability]
  boost3_Other = [_Ability]
  flaw_Other = [_Ability]

  boost_Other = [_Ability]
  boost_Alchemist = .Int
  boost_Barbarian = .Str
  boost_Bard = .Cha

  _Proficiency is .U .T .E .M .L

  prof_U = 0
  prof_T = Level + 2
  prof_E = Level + 4
  prof_M = Level + 6
  prof_L = Level + 8
}
`
