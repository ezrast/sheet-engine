open Webapi;

@bs.module("../js-modules/preact.module.js") external ce: (string, 'b, array<'c>) => 'd = "createElement"
@bs.module("../js-modules/preact.module.js") external preactRender: ('a, Dom.Element.t) => 'd = "render"
@bs.new external codeMirror: ('a, 'b) => 'c = "CodeMirror"
@bs.get external unsafeGetValue : ('a) => string = "value";

let inputBuffer = ref(Source.source)
let onSourceUpdate = (cm, _) => {
  inputBuffer := cm["getDoc"]()["getValue"]()
}
let rec renderSheet = (sheet: Sheet.sheet, rootElement) => {
  let formInputEventHandler = event => {
    let target = event |> Dom.InputEvent.target |> Dom.EventTarget.unsafeAsElement
    switch target |> Dom.Element.getAttribute("data-label") {
    | Some(label) =>
      let value = event |> Dom.InputEvent.target |> unsafeGetValue
      sheet.formSymbolInputs->Belt.HashMap.String.set(label, value)
      renderSheet(sheet, rootElement)
    | None => Js.Console.error("No label found on input event")
    }
  }

  let numberInputEventHandler = event => {
    let target = event |> Dom.InputEvent.target |> Dom.EventTarget.unsafeAsElement
    switch target |> Dom.Element.getAttribute("data-label") {
    | Some(label) =>
      let value = Sheet.toIntOrZero(Dom.InputEvent.data(event))
      sheet.formNumberInputs->Belt.HashMap.String.set(label, value)
      renderSheet(sheet, rootElement)
    | None => Js.Console.error("No label found on input event")
    }
  }

  let renderValue = (label, tokens) => {
    switch Sheet.evaluate(tokens, sheet, ~debug=(Js.Array.includes(label, sheet.debug))) {
    | Ok(Number(ii)) =>
      ce("span", {"id": label ++ "-value-inner"}, [Belt.Int.toString(ii)])
    | Ok(Symbol(ss)) =>
      ce("span", {"id": label ++ "-value-inner"}, [ss])
    | Ok(NumberForm(label)) =>
      let value = Sheet.getNumberInput(sheet, label)
      ce("input", {"id": label ++ "-value-inner", "type": "number", "value": Belt.Int.toString(value), "onInput": numberInputEventHandler, "data-label": label}, [])
    | Ok(SetForm(label, setName)) =>
      let value = Sheet.getSymbolInput(sheet, label, setName)
      let options = switch Belt.HashMap.String.get(sheet.formulas, setName) {
      | Some(Set(symbols)) =>
        symbols -> Belt.Array.map(symbol => {
          ce("option", {"id": label ++ "-form-input-" ++ symbol, "value": symbol}, [symbol])
        })
      | _ =>
        Js.log("Error: no such set: " ++ setName)
        []
      }

      ce("select", {"id": label ++ "-value-inner", "onInput": formInputEventHandler, "data-label": label}, options)
    | Ok(_) =>
      ce("div", {"id": label ++ "-value-inner"}, ["Err: Impossible! Unexpected evaluate result"])
    | Error(str) =>
      ce("div", {"id": label ++ "-value-inner"}, ["Err: " ++ str])
    }
  }

  let rec renderBlock = (blockName, blockClasses, subItems) => {
    let classString = Js.Array.joinWith(" ", blockClasses)
    let blockContent = ce("div", {"id": "content-" ++ blockName, "class": classString}, renderLayout(subItems))
    let blockChildren = if blockName == "" {
      [blockContent]
    } else {
      [
        ce("h1", {"id": "header-" ++ blockName}, [blockName]),
        blockContent,
      ]
    }
    ce("section", {"id": "section-" ++ blockName}, blockChildren)
  }

  and renderLayout = (layout: array<Sheet.layout>) => {
    layout -> Belt.Array.keepMap((item) =>
      switch item {
      | Block(blockName, blockClasses, subItems) =>
        if Js.String.startsWith("_", blockName) {
          None
        } else {
          Some(renderBlock(blockName, blockClasses, subItems))
        }
      | Label(label, displayLabel) =>
        switch Belt.HashMap.String.get(sheet.formulas, label) {
        | Some(Variable(tokens)) =>
            Some(ce("div", {"id": label, "class": "variable"}, [
              ce("div", {"id": label ++ "-label", "class": "label"}, [displayLabel->Belt.Option.getWithDefault(label)]),
              ce("div", {"id": label ++ "-value", "class": "value"}, renderValue(label, tokens))
            ]))
        | Some(Set(symbols)) => None
        | None =>
          Js.log("Impossible! Missing label " ++ label)
          None
        }
      }
    )
  }

  preactRender(renderLayout(sheet.layout), rootElement)
}

let render = () => {
  let maybeSourceRoot = Dom.Document.getElementById("sourceRoot", Dom.document)
  let maybeSheetRoot = Dom.Document.getElementById("sheetRoot", Dom.document)

  switch [maybeSourceRoot, maybeSheetRoot] {
  | [None, None] => Js.Console.error("Missing elements: sourceRoot, sheetRoot. Is the HTML well-formed?")
  | [None, _] => Js.Console.error("Missing element: sourceRoot. Is the HTML well-formed?")
  | [_, None] => Js.Console.error("Missing element: sheetRoot. Is the HTML well-formed?")
  | [Some(sourceRoot), Some(sheetRoot)] =>
    let cm = codeMirror(sourceRoot, {
      "lineNumbers": true,
      "showCursorWhenSelecting": true,
      "autofocus": true,
      "value": Source.source,
    })

    cm["on"]("change", onSourceUpdate)
    cm["refresh"]()

    let parseButton = Dom.Document.createElement("input", Dom.document)
    Dom.Element.setAttribute("type", "button", parseButton)
    Dom.Element.setAttribute("value", "Parse", parseButton)

    let onParseClick = (event) => {
      switch Sheet.parse(inputBuffer.contents) {
      | Ok(sheet) => renderSheet(sheet, sheetRoot)
      | Error(msg) => Js.log("Err: " ++ msg)
      }
    }
    Dom.Element.setOnClick(parseButton, onParseClick)
    Dom.Element.appendChild(parseButton, sourceRoot)
  }
}
