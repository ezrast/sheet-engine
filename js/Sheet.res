type token =
  | Dice(int, int)
  | Number(int)
  | Op1(string)
  | Op2(string)
  | Cmp
  | LParen
  | RParen
  | Space
  | Symbol(string)
  | Interp(string)
  | NumberForm(string)
  | SetForm(string, string)

type formula =
  | Variable(array<token>)
  | Set(array<string>)

type formulaMapEntry = {
  string: string,
  displayString: option<string>,
  formula: formula,
}

type rec layout =
  | Block(string, array<string>, array<layout>)
  | Label(string, option<string>)

let rec forEachVariable = (layouts: array<layout>, func) => {
  layouts -> Belt.Array.forEach(layout => {
    switch layout {
    | Block(name, _, subItems) => forEachVariable(subItems, func)
    | Label(label, _) => func(label)
    }
  })
}

type sheet = {
  formulas: Belt.HashMap.String.t<formula>,
  layout: array<layout>,
  formNumberInputs: Belt.HashMap.String.t<int>,
  formSymbolInputs: Belt.HashMap.String.t<string>,

  mutable debug: array<string>,
}

let newSheet = (size) => {
  formulas: Belt.HashMap.String.make(~hintSize=size),
  layout: [],
  formNumberInputs: Belt.HashMap.String.make(~hintSize=size),
  formSymbolInputs: Belt.HashMap.String.make(~hintSize=size),
  debug: [],
}

let identPat = "(?:[a-zA-Z0-9_]+)"
let toIntOrZero = str => Belt.Int.fromString(str)->Belt.Option.getWithDefault(0)

type tokenParser = { pat: Js.Re.t, func: (array<string>, string) => token}
let tokenPatterns = [
  { pat: %re("/^(\d?)d(\d+)/"), func: (mm, _) => {Dice(toIntOrZero(mm[1]), toIntOrZero(mm[2]))} },
  { pat: %re("/^\d+/"), func: (mm, _) => {Number(toIntOrZero(mm[0]))} },
  { pat: %re("/^[*\/]/"), func: (mm, _) => {Op1(mm[0])} },
  { pat: %re("/^[-+]/"), func: (mm, _) => {Op2(mm[0])} },
  { pat: %re("/^==/"), func: (mm, _) => {Cmp} },
  { pat: %re("/^\(/"), func: (mm, _) => {LParen} },
  { pat: %re("/^\)/"), func: (mm, _) => {RParen} },
  { pat: %re("/^\s+/"), func: (mm, _) => {Space} },
  { pat: Js.Re.fromString("^\.(" ++ identPat ++ ")"), func: (mm, _) => {Symbol(mm[1])} },
  { pat: %re("/^[a-zA-Z0-9\{\}_]+/"), func: (mm, _) => {Interp(mm[0])} },
  { pat: %re("/^\[\]/"), func: (mm, label) => {NumberForm(label)} },
  { pat: Js.Re.fromString("^\[(" ++ identPat ++ ")\]"), func: (mm, label) => {SetForm(label, mm[1])} },
]

let tokenize = (str, label): result<array<token>, string> => {
  let ret = []

  let rec loop = (idx, str) => {
    if idx >= Belt.Array.length(tokenPatterns) {
      Some("Could not parse: " ++ str)
    } else if str == "" {
      None
    } else {
      let { pat, func } = tokenPatterns[idx]
      switch Js.String.match_(pat, str) {
      | Some(match) =>
        switch func(match, label) {
        | Space => ()
        | token => ignore(Js.Array.push(token, ret))
        }
        let remaining_str = Js.String.sliceToEnd(~from=Js.String.length(match[0]), str)
        loop(0, remaining_str)
      | None => loop(idx + 1, str)
      }
    }
  }

  switch loop(0, str) {
  | Some(err) => Error(err)
  | None => Ok(ret)
  }
}

type formulaParser = { pat: Js.Re.t, func: array<string> => result<formulaMapEntry, string>}
let variableParser = {
  pat: Js.Re.fromString("^(" ++ (identPat ++ ")\s*((?:\"[^\"]*\")?)\s*?=(.*)")),
  func: match => {
    switch tokenize(match[3], match[1]) {
    | Ok(tokens) =>
      let displayString = if match[2] == "" {
        None
      } else {
        Some(Js.String.slice(~from=1, ~to_=(Js.String.length(match[2])-1), match[2]))
      }
      Ok({
        string: match[1],
        displayString: displayString,
        formula: Variable(tokens)
      })
    | Error(msg) => Error(msg)
    }
  },
}

let setParser = {
  pat: Js.Re.fromString("^(" ++ (identPat ++ ")\s*is((?:\s+\." ++ identPat ++ ")+)$")),
  func: match => {
    let symbols = (
      Js.String.split(" ", match[2])
      ->Belt.Array.keep(str => str != "")
      ->Belt.Array.map(Js.String.sliceToEnd(~from=1))
    )
    Ok({string: match[1], displayString: None, formula: Set(symbols)})
  },
}

let parseLine = line => {
  let content = Js.String.trim(Js.String.splitAtMost("#", ~limit=1, line)[0])
  if content == "" {
    None
  } else {
    let tryParse = (parser, fallback) => {
      switch Js.String.match_(parser.pat, content) {
      | Some(match) => Some(parser.func(match))
      | None => fallback()
      }
    }
    tryParse(variableParser, () =>
      tryParse(setParser, () => Some(Error("Could not parse: " ++ content)))
    )
  }
}

let blockStartPat = Js.Re.fromString("^\s*([a-zA-Z0-9_.]+)\s*{\s*$")

let parse = (source: string) => {
  let lines = Js.String.split("\n", source)

  let sheet = newSheet(Belt.Array.length(lines))
  let bad = []

  let blockStack: array<array<layout>> = [sheet.layout]
  let topBlock = () => blockStack[Belt.Array.length(blockStack) - 1]

  lines->Belt.Array.forEach(line =>
    if Js.String.startsWith("@debug ", line) {
      sheet.debug = Js.String.split(" ", line)
    } else if Js.String.trim(line) == "}" {
      if Belt.Array.length(blockStack) > 1 {
        ignore(Js.Array.pop(blockStack))
      } else {
        ignore(Js.Array.push((line, "Unmatched close brace"), bad))
      }
    } else {
      switch Js.String.match_(blockStartPat, line) {
      | Some(mm) =>
        let nextBlockArr: array<layout> = []

        let parts = Js.String.split(".", mm[1])
        let blockId = parts->Belt.Array.get(0)->Belt.Option.getWithDefault("")
        let classes = Belt.Array.sliceToEnd(parts, 1)
        ignore(Js.Array.push(Block(blockId, classes, nextBlockArr), topBlock()))
        ignore(Js.Array.push(nextBlockArr, blockStack))
      | None =>
        switch parseLine(line) {
        | Some(Ok(entry)) =>
          Belt.HashMap.String.set(sheet.formulas, entry.string, entry.formula)
          ignore(Js.Array.push(Label(entry.string, entry.displayString), topBlock()))
        | None => ()
        | Some(Error(msg)) => ignore(Js.Array.push((line, msg), bad))
        }
      }
    }
  )

  if Belt.Array.length(bad) == 0 {
    Ok(sheet)
  } else {
    Error(Js.Array.joinWith("\n", Belt.Array.map(bad, ((_, msg)) => msg)))
  }
}

let tokenToString = token => {
  switch token {
  | Dice(count, sides) => string_of_int(count) ++ "d" ++ string_of_int(sides)
  | Number(int) => string_of_int(int)
  | Op1(string) => "`" ++ string ++ "`"
  | Op2(string) => "`" ++ string ++ "`"
  | Cmp => "=="
  | LParen => "("
  | RParen => ")"
  | Space => " "
  | Symbol(string) => "." ++ string
  | Interp(string) => "\"" ++ string ++ "\""
  | NumberForm(label) => "[]"
  | SetForm(label, setName) => "[" ++ setName ++ "]"
  }
}

let tokensToString = tokens => {
  tokens -> Belt.Array.map(tokenToString)
  |> Js.Array.joinWith(", ")
}

let getNumberInput = (sheet, label) => {
  Belt.HashMap.String.get(sheet.formNumberInputs, label)
  ->Belt.Option.getWithDefault(0)
}

let getSymbolInput = (sheet, label, setName) => {
  Belt.HashMap.String.get(sheet.formSymbolInputs, label)
  ->Belt.Option.getWithDefault(
    switch Belt.HashMap.String.get(sheet.formulas, setName) {
    | Some(Set(items)) => items[0]
    | _ =>
      Js.log("Error! getSymbolInput couldn't find set " ++ label)
      ""
    }
  )
}

let evaluate = (tokens, sheet, ~debug=false) => {
  let iterations = ref(100)
  let memo: Belt.HashMap.String.t<token> = Belt.HashMap.String.make(
    ~hintSize=Belt.HashMap.String.size(sheet.formulas)
  )

  let debugTokens = if debug {
    tokens => Js.log(tokensToString(tokens))
  } else {
    _ => ()
  }

  let debugObj = if debug {
    obj => Js.log(obj)
  } else {
    _ => ()
  }

  let rec _evaluate = (origTokens: array<token>) => {
    let ret: result<token, string> = switch origTokens {
    | [Number(xx)] =>
      Ok(Number(xx))
    | [Symbol(ss)] =>
      Ok(Symbol(ss))
    | [NumberForm(label)] =>
      Ok(NumberForm(label))
    | [SetForm(label, setName)] =>
      Ok(SetForm(label, setName))
    | _ =>
      let tokens = Belt.Array.copy(origTokens)
      if iterations.contents <= 0 {
        Error("Too many iterations; infinite expansion?")
      } else {
        iterations := iterations.contents - 1
        let steps = [
          resolveInterp,
          resolveVariable,
          resolveParens,
          resolveOp1,
          resolveOp2,
          resolveCmp,
        ]
        let rec loop = (idx) => {
          if idx >= Belt.Array.length(steps) {
            Error("Could not simplify expression: " ++ tokensToString(tokens))
          } else {
            switch steps[idx](tokens) {
            | Some(Ok(newTokens)) =>
              debugObj(tokensToString(origTokens) ++ " becomes " ++ tokensToString(newTokens))
              _evaluate(newTokens)
            | Some(Error(msg)) => Error(msg)
            | None => loop(idx + 1)
            }
          }
        }
        loop(0)
      }
    }
    ret
  }

  and resolveInterp = tokens => {
    let rec findMatchedBraces = (idx: int) => {
      switch Belt.Array.get(tokens, idx) {
      | Some(Interp(string)) =>
        let rec loop2 = (lpos, idx2) => {
          switch Js.String.charAt(idx2, string) {
          | "{" => loop2(Some(idx2), idx2 + 1)
          | "}" => switch lpos {
            | Some(lpos) =>
              let substr = Js.String.slice(string, ~from=lpos+1, ~to_=idx2)
              switch tokenize(substr, "") {
              | Ok(interiorTokens) =>
                switch _evaluate(interiorTokens) {
                | Ok(Symbol(part2)) =>
                  let part1 = Js.String.substring(string, ~from=0, ~to_=lpos, )
                  let part3 = Js.String.substringToEnd(string, ~from=idx2+1)
                  let newInterp = Interp(part1 ++ part2 ++ part3)
                  ignore(Belt.Array.set(tokens, idx, newInterp))
                  Some(Ok(tokens))
                | Ok(Number(ii)) => Some(Error("Interpolation must evaluate to symbol"))
                | Ok(NumberForm(_)) => Some(Error("Interpolation must evaluate to symbol"))
                | Ok(SetForm(label, setName)) =>
                  let part2 = getSymbolInput(sheet, label, setName)
                  let part1 = Js.String.substring(string, ~from=0, ~to_=lpos, )
                  let part3 = Js.String.substringToEnd(string, ~from=idx2+1)
                  let newInterp = Interp(part1 ++ part2 ++ part3)
                  ignore(Belt.Array.set(tokens, idx, newInterp))
                  Some(Ok(tokens))
                | Ok(_) => Some(Error("Impossible! evaluate did not return number or symbol"))
                | Error(msg) => Some(Error(msg))
                }
              | Error(msg) => Some(Error(msg))
              }
            | None => Some(Error("Unmatched braces in " ++ string))
            }
          | "" => switch lpos {
            | Some(_) => Some(Error("Unmatched braces in " ++ string))
            | None => None
            }
          | x => loop2(lpos, idx2 + 1)
          }
        }
        loop2(None, 0)
      | Some(_) => findMatchedBraces(idx + 1)
      | None => None
      }
    }
    findMatchedBraces(0)
  }

  and resolveVariable = tokens => {
    let rec loop = (idx): option<result<array<token>, string>> => {
      switch Belt.Array.get(tokens, idx) {
      | Some(Interp(variableName)) =>
        let tokenToSubstitute = switch Belt.HashMap.String.get(memo, variableName) {
        | Some(memorizedValue) =>
          Ok(memorizedValue)
        | None =>
          switch Belt.HashMap.String.get(sheet.formulas, variableName) {
          | Some(Variable(tokens)) =>
            switch _evaluate(tokens) {
            | Ok(answer) =>
              Belt.HashMap.String.set(memo, variableName, answer)
              Ok(answer)
            | xx => xx
            }
          | None => Error("No form: " ++ variableName)
          }
        }
        switch tokenToSubstitute {
        | Error(msg) =>
          Some(Error(msg))
        | Ok(token) =>
          if Belt.Array.set(tokens, idx, token) {
            Some(Ok(tokens))
          } else {
            Some(Error("Impossible! Could not update token index " ++ Belt.Int.toString(idx)))
          }
        }
      | None => None
      | _ => loop(idx + 1)
      }
    }
    loop(0)
  }

  and resolveParens = tokens => {
    let rec findMatchedParens = (lpos, idx) => {
      switch Belt.Array.get(tokens, idx) {
      | Some(LParen) => findMatchedParens(Some(idx), idx + 1)
      | Some(RParen) => switch lpos {
        | Some(lpos) =>
          let subarr = Js.Array.slice(tokens, ~start=lpos+1, ~end_=idx)
          switch _evaluate(subarr) {
          | Ok(tokenToSubstitute) =>
            let part1 = Js.Array.slice(tokens, ~start=0, ~end_=lpos)
            let part3 = Belt.Array.sliceToEnd(tokens, idx+1)
            let newTokens = Belt.Array.concatMany([part1, [tokenToSubstitute], part3])
            Some(Ok(newTokens))
          | Error(msg) => Some(Error(msg))
          }
        | None => Some(Error("Unmatched parentheses in " ++ tokensToString(tokens)))
        }
      | None => switch lpos {
        | Some(_) => Some(Error("Unmatched parentheses in " ++ tokensToString(tokens)))
        | None => None
        }
      | _ => findMatchedParens(lpos, idx + 1)
      }
    }
    findMatchedParens(None, 0)
  }

  and resolveOp1 = tokens => {
    let lastIdx = Belt.Array.length(tokens) - 1
    let rec loop = idx => {
      if idx >= lastIdx {
        None
      } else {
        switch tokens[idx] {
        | Op1(op) =>
          let numberA = switch tokens[idx - 1] {
          | NumberForm(label) => Number(getNumberInput(sheet, label))
          | xx => xx
          }
          let numberB = switch tokens[idx + 1] {
          | NumberForm(label) => Number(getNumberInput(sheet, label))
          | xx => xx
          }
          let substituteToken = switch (numberA, op, numberB) {
          | (Number(aa), "*", Number(bb)) => Ok(Number(aa * bb))
          | (Number(aa), "/", Number(bb)) => Ok(Number(aa / bb))
          | (aa, _, bb) => Error("Operator " ++ op ++ " must be between two numbers; got " ++ tokenToString(aa) ++ " and " ++ tokenToString(bb))
          }
          switch substituteToken {
          | Ok(substituteToken) =>
            ignore(Js.Array.removeCountInPlace(~pos=idx, ~count=2, tokens))
            ignore(Belt.Array.set(tokens, idx - 1, substituteToken))
            Some(Ok(tokens))
          | Error(msg) => Some(Error(msg))
          }
        | _ => loop(idx + 1)
        }
      }
    }
    loop(1)
  }

  and resolveOp2 = tokens => {
    let lastIdx = Belt.Array.length(tokens) - 1
    let rec loop = idx => {
      if idx >= lastIdx {
        None
      } else {
        switch tokens[idx] {
        | Op2(op) =>
          let numberA = switch tokens[idx - 1] {
          | NumberForm(label) => Number(getNumberInput(sheet, label))
          | xx => xx
          }
          let numberB = switch tokens[idx + 1] {
          | NumberForm(label) => Number(getNumberInput(sheet, label))
          | xx => xx
          }
          let substituteToken = switch (numberA, op, numberB) {
          | (Number(aa), "+", Number(bb)) => Ok(Number(aa + bb))
          | (Number(aa), "-", Number(bb)) => Ok(Number(aa - bb))
          | (aa, _, bb) => Error("Operator " ++ op ++ " must be between two numbers; got " ++ tokenToString(aa) ++ " and " ++ tokenToString(bb))
          }
          switch substituteToken {
          | Ok(substituteToken) =>
            ignore(Js.Array.removeCountInPlace(~pos=idx, ~count=2, tokens))
            ignore(Belt.Array.set(tokens, idx - 1, substituteToken))
            Some(Ok(tokens))
          | Error(msg) => Some(Error(msg))
          }
        | _ => loop(idx + 1)
        }
      }
    }
    loop(1)
  }

  and resolveCmp = tokens => {
    let lastIdx = Belt.Array.length(tokens) - 1
    if tokens[0] == Cmp || tokens[lastIdx] == Cmp {
      Some(Error("`==` must not be at beginning or ending of expression"))
    } else {
      let rec loop = idx => {
        if idx >= lastIdx {
          None
        } else {
          switch tokens[idx] {
          | Cmp =>
            let valueA = switch tokens[idx - 1] {
            | NumberForm(label) => Number(getNumberInput(sheet, label))
            | SetForm(label, setName) => Symbol(getSymbolInput(sheet, label, setName))
            | xx => xx
            }
            let valueB = switch tokens[idx + 1] {
            | NumberForm(label) => Number(getNumberInput(sheet, label))
            | SetForm(label, setName) => Symbol(getSymbolInput(sheet, label, setName))
            | xx => xx
            }
            let substituteToken = switch (valueA, valueB) {
            | (Number(aa), Number(bb)) when aa == bb => Number(1)
            | (Symbol(aa), Symbol(bb)) when aa == bb => Number(1)
            | _ => Number(0)
            }
            ignore(Js.Array.removeCountInPlace(~pos=idx, ~count=2, tokens))
            ignore(Belt.Array.set(tokens, idx - 1, substituteToken))
            Some(Ok(tokens))
          | _ => loop(idx + 1)
          }
        }
      }
      loop(1)
    }
  }

  _evaluate(tokens)
}
