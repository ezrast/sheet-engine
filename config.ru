#!/usr/bin/env rackup
#\ -E deployment

# Simple dev server to be used in conjunction with `yarn run watch`.
# Run `rackup` in the project directory to use.
# Relying on the Ruby ecosystem for this makes no sense but I have no
# idea what I'm doing in JS-land. /shrug

use Rack::ContentLength

app = Rack::Directory.new(Dir.pwd + '/html')
run app
